/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

Copyright (C) 2013 Fabian Homborg <FHomborg@gmail.com>
based on spirallayout.js by Matthias Gottschlag

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

// FIXME: Neighbour stuff is bogus (just copied from spirallayout)
// FIXME: Crash on moving client to another desktop
/**
 * Class which arranges the windows in a spiral with the largest window filling
 * the left half of the screen.
 */
function ValfLayout(screenRectangle) {
    Layout.call(this, screenRectangle);
		this.name = "Valf"
	this.firstHeight = Math.floor(this.screenRectangle.height / 2);
	this.master = 0;
}


ValfLayout.prototype = new Layout();
ValfLayout.prototype.constructor = ValfLayout;

ValfLayout.prototype.name = "Valf";
// TODO: Add an image for the layout switcher
ValfLayout.prototype.image = null;

ValfLayout.prototype.resetTileSizes = function() {
	try {
		// Simply erase all tiles and recreate them to recompute the initial sizes
		var tileCount = this.tiles.length;
		this.tiles.length = 0;
		for (var i = 0; i < tileCount; i++) {
			this.addTile();
		}
	} catch(err) {
		print(err, "in ValfLayout.resetTileSizes");
	}
}

ValfLayout.prototype.addTile = function() {
	try {
		if (this.tiles.length == 0) {
			// The first tile fills the whole screen
			var rect = Qt.rect(this.screenRectangle.x,
							   this.screenRectangle.y,
							   this.screenRectangle.width,
							   this.screenRectangle.height);
			this._createTile(rect);
			return;
		} 
		if (this.tiles.length == 1) {
			// The second tile fills the right half of the screen
			// Also, javascript sucks
			var firstRect = Qt.rect(this.tiles[0].rectangle.x,
									this.tiles[0].rectangle.y,
									this.tiles[0].rectangle.width,
									this.firstHeight);
			this.tiles[0].rectangle = firstRect;
			var newRect = Qt.rect(firstRect.x,
								  firstRect.y + firstRect.height,
									firstRect.height,
								  this.screenRectangle.height - firstRect.height)
			this._createTile(newRect);
			return;
		}
		if (this.tiles.length > 1) {
			// Every other tile separates the right half
			var lastRect = this.tiles[0].rectangle;
			var newRect = Qt.rect(lastRect.x,
								  lastRect.y + lastRect.height,
									Math.floor(lastRect.width / (this.tiles.length)),
								  this.screenRectangle.height - lastRect.height);
			newRect.x = newRect.x + newRect.width * (this.tiles.length - 1);
			// FIXME: Try to keep ratio
			for (var i = 1; i < this.tiles.length; i++) {
				var rect = this.tiles[i].rectangle;
				rect.y = newRect.y;
				var offset = newRect.width * (i - 1);
				rect.x = lastRect.x + offset;
				rect.width = newRect.width;
				rect.height = newRect.height;
				this.tiles[i].rectangle = rect;
			}
			// Adjust lowest tile's height for rounding errors
			//newRect.y = newRect.y + newRect.width * (this.tiles.length - 1);
			newRect.width = (this.screenRectangle.x + this.screenRectangle.width) - newRect.x;
			this._createTile(newRect);
		}
	} catch(err) {
		print(err, "in ValfLayout.addTile");
	}
}

ValfLayout.prototype.removeTile = function(tileIndex) {
	try {
		//FIXME: There is a crash here
		// Remove the array entry
		var oldrect = this.tiles[tileIndex].rectangle;
		if (tileIndex < 0 || tileIndex >= this.tiles.length) {
			print("Removing invalid tileindex");
			return;
		}
		this.tiles.splice(tileIndex, 1);
		// Update the other tiles
		if (this.tiles.length == 1) {
			this.tiles[0].rectangle = this.screenRectangle;
			this.tiles[0].hasDirectNeighbour[Direction.Left] = false;
			this.tiles[0].hasDirectNeighbour[Direction.Right] = false;
			this.tiles[0].hasDirectNeighbour[Direction.Up] = false;
			this.tiles[0].hasDirectNeighbour[Direction.Down] = false;
		}
		if (this.tiles.length > 1) {
			if (tileIndex == 0) {
				this.tiles[0].rectangle = oldrect;
				this.tiles[0].hasDirectNeighbour[Direction.Left] = false;
				this.tiles[0].hasDirectNeighbour[Direction.Right] = false;
				this.tiles[0].hasDirectNeighbour[Direction.Up] = false;
				this.tiles[0].hasDirectNeighbour[Direction.Down] = true;
				this.tiles[0].neighbours[Direction.Down] = 1;
			}
			var tileCount = this.tiles.length - 1;
			var lastRect = this.tiles[0].rectangle;
			var newRect = Qt.rect(lastRect.x,
								  lastRect.height,
									Math.floor(lastRect.width / tileCount),
								  lastRect.height);
			var lowest = 1;
			for (var i = 1; i < this.tiles.length; i++) {
				var rect = this.tiles[i].rectangle;
				rect.x = newRect.x + newRect.width * (i - 1);
				rect.width = newRect.width;
				this.tiles[i].rectangle = rect;
				if (this.tiles[lowest].rectangle.x < this.tiles[i].rectangle.x) {
					lowest = i;
				}
				this.tiles[i].hasDirectNeighbour[Direction.Up] = true;
				this.tiles[i].hasDirectNeighbour[Direction.Down] = false;
				if (i == 1) {
					this.tiles[i].hasDirectNeighbour[Direction.Left] = false;
				} else {
					this.tiles[i].hasDirectNeighbour[Direction.Left] = true;
				}
				if (i == this.tiles.length - 1) {
					this.tiles[i].hasDirectNeighbour[Direction.Right] = false;
				} else {
					this.tiles[i].hasDirectNeighbour[Direction.Right] = true;
				}
				this.tiles[i].neighbours[Direction.Up] = 0;
				this.tiles[i].neighbours[Direction.Left] = i - 1;
				this.tiles[i].neighbours[Direction.Right] = i + 1;
			}
			// Adjust lowest tile's height for rounding errors
			this.tiles[lowest].rectangle.width = (this.screenRectangle.x + this.screenRectangle.width) - this.tiles[lowest].rectangle.x;
		}
	} catch(err) {
		print(err, "in ValfLayout.removeTile");
	}
}

ValfLayout.prototype.resizeTile = function(tileIndex, rectangle) {
	try {
		// TODO: Mark tile as resized so it can keep its size on tileadd/remove
		if (tileIndex < 0 || tileIndex > (this.tiles.length - 1)) {
			print("Tileindex invalid", tileIndex, "/", this.tiles.length);
			return;
		}
		var tile = this.tiles[tileIndex];
		if (tile == null) {
			print("No tile");
			return;
		}
		if (rectangle == null){
			print("No rect");
			return;
		}
		// Don't allow resizing away from the screenedges
		// except when one client has squashed others
		var oldRect = this.tiles[tileIndex].rectangle;
		if (oldRect.y == this.screenRectangle.y) {
			// The non-first clients can cover the entire screen (and let the first client be invisible)
			// Allow moving them back
			if (tileIndex == 0) {
				// This assumes that the left and right window edge can't change at the same time
				if (rectangle.y != oldRect.y) {
					rectangle.y = this.screenRectangle.y;
					rectangle.height = oldRect.height;
				}
			}
		}
		if (oldRect.x == this.screenRectangle.x) {
			// Tile 0 and 1 are at the upper edge
			// If another tile is, it squashes tile 1 (and maybe others)
			if (tileIndex < 2) {
				if (rectangle.x != oldRect.x) {
					rectangle.x = this.screenRectangle.x;
					rectangle.width = oldRect.width;
				}
			}
		}
		if (oldRect.x + oldRect.width == this.screenRectangle.x + this.screenRectangle.width) {
			// First tile or bottom tile
			if (tileIndex == this.tiles.length - 1 || tileIndex == 0) {
				rectangle.width = (this.screenRectangle.x + this.screenRectangle.width) - rectangle.x;
			}
		}
		if (oldRect.y + oldRect.height == this.screenRectangle.y + this.screenRectangle.height) {
			// The first client can cover the entire screen even when there are other clients present
			// Allow moving it back
			if (tileIndex != 0 || this.tiles.length == 1) {
				rectangle.height = (this.screenRectangle.y + this.screenRectangle.height) - rectangle.y;
			}
		}

		if (tileIndex == 0) {
			// Simply adjust width on everything else, no height adjustment
			// Only adjust when it's different (otherwise firstWidth gets clobbered)
			if (rectangle.height != tile.rectangle.height) {
				this.firstHeight = rectangle.height;
				tile.rectangle = rectangle;
				for (var i = 1; i < this.tiles.length; i++) {
					this.tiles[i].rectangle.height = this.screenRectangle.height - rectangle.height;
					this.tiles[i].rectangle.y = this.screenRectangle.y + rectangle.height;
				}
			}
		} else {
			this.tiles[0].rectangle.height = rectangle.y - this.screenRectangle.y;
			this.firstHeight = this.tiles[0].rectangle.height;
			var belows = new Array();
			var aboves = new Array();
			for (var i = 1; i < this.tiles.length; i++) {
				if (this.tiles[i].rectangle.x < tile.rectangle.x){
					aboves.push(i);
				}
				if (this.tiles[i].rectangle.x > tile.rectangle.x){
					belows.push(i);
				}
			}
			// Dividing by zero sucks
			if (aboves.length == 0) {
				var newWidthAbove = 0;
			} else {
				var newWidthAbove = Math.floor((rectangle.x - this.screenRectangle.x) / aboves.length);
			}
			if (belows.length == 0) {
				var newHeightBelow = 0;
			} else {
				var newWidthBelow = Math.floor((this.screenRectangle.width - rectangle.width) / belows.length);
			}
			if (belows.length > 0) {
				for (var i = 0; i < belows.length; i++) {
					this.tiles[belows[i]].rectangle.height = rectangle.height;
					this.tiles[belows[i]].rectangle.y = rectangle.y;
					this.tiles[belows[i]].rectangle.x = rectangle.x + rectangle.width + newWidthBelow * i;
					this.tiles[belows[i]].rectangle.height = newWidthBelow;
				}
			}
			if (aboves.length > 0) {
				for (var i = 0; i < aboves.length; i++) {
					this.tiles[aboves[i]].rectangle.height = rectangle.height;
					this.tiles[aboves[i]].rectangle.y = rectangle.y;
					this.tiles[aboves[i]].rectangle.x = this.screenRectangle.x + newWidthAbove * i;
					this.tiles[aboves[i]].rectangle.width = newWidthAbove;
				}
			}
			tile.rectangle = rectangle;
		}
	} catch(err) {
		print(err, "in ValfLayout.resizeTile");
	}
}

ValfLayout.prototype._createTile = function(rect) {
	try {
		// Update the last tile in the list
		if (this.tiles.length > 1) {
			var lastTile = this.tiles[this.tiles.length - 1];
			lastTile.neighbours[Direction.Left] = this.tiles.length;
			lastTile.hasDirectNeighbour[Direction.Left] = true;
		}
		
		if (this.tiles.length == 1) {
			var lastTile2 = this.tiles[0];
			lastTile2.neighbours[Direction.Down] = 1;
			lastTile2.hasDirectNeighbour[Direction.Down] = true;
		}
		// Create a new tile and add it to the list
		var tile = {};
		tile.rectangle = rect;
		tile.neighbours = [];
		tile.hasDirectNeighbour = [];
		tile.neighbours[Direction.Up] = 0;
		tile.hasDirectNeighbour[Direction.Up] = (this.tiles.length > 0);
		tile.neighbours[Direction.Down] = - 1;
		tile.hasDirectNeighbour[Direction.Down] = false;
		if (this.tiles.length > 1) {
			tile.hasDirectNeighbour[Direction.Right] = true;
			tile.neighbours[Direction.Right] = this.tiles.length - 1;
		} else {
			if (this.tiles.length == 0) {
				tile.hasDirectNeighbour[Direction.Right] = false;
				tile.neighbours[Direction.Right] = - 1;
			}
		}
		tile.neighbours[Direction.Left] = - 1;
		tile.hasDirectNeighbour[Direction.Left] = false;
		tile.index = this.tiles.length;
		this.tiles.push(tile);
	} catch(err) {
		print(err, "in ValfLayout._createTile");
	}
}
