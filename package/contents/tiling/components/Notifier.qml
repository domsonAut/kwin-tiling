import QtQuick 1.1;
import org.kde.plasma.core 0.1 as PlasmaCore;


Item {
	id: root
	width: 0
	height: 0
	
	PlasmaCore.Dialog {
		id: dialog
		visible: false
		focus: false
		windowFlags: Qt.Popup | Qt.X11BypassWindowManagerHint
		
		signal isCompleted
		signal tileAdded (variant tile)
		signal tileRemoved (variant tile)
		signal tileResized (variant tile)
		signal tileActivityChanged (variant tile)
		signal tileScreenChanged (variant tile)
		signal tileCaptionChanged (variant tile)
		signal tilePositionChanged (variant client)
		signal tilePositionSet (variant tile)
		signal tilingActivated (variant tiling)
		signal currentDesktopChanged (int desktop)
		signal currentActivityChanged (int activity)
		signal currentScreenChanged (int screen)
		
		
		mainItem: Item {
			id: notifier
			width: Math.max(root.width / 5, 100)
			height: Math.max(root.height / 10, 50)
			focus: false
			
			Plasma.Label {id: topic; focus: false; text: "no text"	
				anchors {
					horizontalCenter: parent.horizontalCenter
					top: parent.top
					margins: 5
				}
			}
			Plasma.Label {id: title; focus: false; text: "no text"
				anchors {
					horizontalCenter: parent.horizontalCenter
					verticalCenter: parent.verticalCenter
					margins: 5
				}
			}
			Plasma.Label {id: subtitle; focus: false; text: "no text"
				anchors {
					horizontalCenter: parent.horizontalCenter
					bottom: parent.bottom
					margins: 5
				}
			}
		}
		
		Timer {
			id: visibleSwitch
			interval: 1500; running: false; repeat: false
			onTriggered: dialog.visible = false
		}
		onVisibleChanged: {
			dialog.focus = false;
			visibleSwitch.start()
		}
	}
	
	function show(top, tit, sub) {
		topic.text = top;
		title.text = tit;
		subtitle.text = sub;
		dialog.visible = true;
	}
	
	Component.onCompleted: {
		root.width = parent.width;
		root.height = parent.height;
	}
}