import QtQuick 1.1;
import org.kde.plasma.core 0.1 as PlasmaCore;
import org.kde.plasma.components 0.1 as Plasma;
import org.kde.qtextracomponents 0.1 as QtExtra;
import org.kde.kwin 0.1;
import "../../code/tilingmanager.js" as Tiling


Item {
	id: root
	width: 0
	height: 0
	
	signal tileAdded (variant tile)
	signal tileRemoved (variant tile)
	signal tileResized (variant tile)
	signal tileActivityChanged (variant tile)
	signal tileScreenChanged (variant tile)
	signal tileCaptionChanged (variant tile)
	signal tilePositionChanged (variant client)
	signal tilePositionSet (variant tile)
	signal tilingActivated (variant tiling)
	signal currentDesktopChanged (int desktop)
	signal currentActivityChanged (int activity)
	signal currentScreenChanged (int screen)
	
	property variant notifier
	
	Component.onCompleted: {
		var screen = workspace.clientArea(KWin.FullScreenArea, workspace.activeScreen, workspace.currentDesktop);
		root.width = screen.width;
		root.height = screen.height;
		
		var MainLogic = new Tiling.TilingManager(root);
		root.notifier = MainLogic.qmlNotifier;
		
		MainLogic.qmlNotifier.tileAdded.connect(root.tileAdded);
		MainLogic.qmlNotifier.tileResized.connect(root.tileResized);
		MainLogic.qmlNotifier.tileRemoved.connect(root.tileRemoved);
		MainLogic.qmlNotifier.tileActivityChanged.connect(root.tileActivityChanged);
		MainLogic.qmlNotifier.tileScreenChanged.connect(root.tileScreenChanged);
		MainLogic.qmlNotifier.tileCaptionChanged.connect(root.tileCaptionChanged);
		MainLogic.qmlNotifier.tilePositionChanged.connect(root.tilePositionChanged);
		MainLogic.qmlNotifier.tilePositionSet.connect(root.tilePositionSet);
		MainLogic.qmlNotifier.tilingActivated.connect(root.tilingActivated);
		MainLogic.qmlNotifier.currentDesktopChanged.connect(root.currentDesktopChanged);
		MainLogic.qmlNotifier.currentScreenChanged.connect(root.currentScreenChanged);
		MainLogic.qmlNotifier.currentActivityChanged.connect(root.currentActivityChanged);
	}

	
	function showdialog() {
		print('Show Dialog');
		dialog.visible = true;
		filterItem.text = "";df
		if (dialog.visible) {
			filterItem.forceActiveFocus();
		}
	}
	
	
	PlasmaCore.Theme {
		id: theme
	}
	
// just to get the margin sizes
	PlasmaCore.FrameSvgItem {
		id: hoverItem
		imagePath: "widgets/viewitem"
		prefix: "hover"
		visible: false
	}
	
	ClientFilterModel {
		id: filterModel
 		clientModel: ClientModel {}
		filter: filterItem.text
	}
	
	VisualDataModel {
		id: clientModel
		model: filterModel
		Component.onCompleted: {
//             clientModel.rootIndex = modelIndex(0);
//             clientModel.rootIndex = modelIndex(0);
		clientModel.delegate = thumbnailDelegate;
		}
	}

	Component {
			id: thumbnailDelegate
			WindowItemComponent {
					view: gridView
					width: gridView.cellWidth
					height: gridView.cellHeight
					standardMargin: 2
					standardLeftMargin: hoverItem.margins.left
					standardRightMargin: hoverItem.margins.right
					standardTopMargin: hoverItem.margins.top
					standardBottomMargin: hoverItem.margins.bottom
					onRequestClose: {
							dialog.visible = false;
					}
			}
	}
	
	function showNotice(top, tit, sub) {
		root.notifier.show(top, tit, sub);
	}
	
	
	PlasmaCore.Dialog {
		id: dialog
		visible: false
		windowFlags: Qt.Popup | Qt.X11BypassWindowManagerHint
		
		
		mainItem: Item {
			id: dialogItem
			width: root.width
			height: root.height
			focus: true
			Plasma.TextField {
				id: filterItem
				focus: true
				clearButtonShown: true
				placeholderText: i18n("Start typing to filter")
				anchors {
					left: parent.left
					right: parent.right
					top: parent.top
				}
			}
			GridView {
				id: gridView
				property int rows: Math.round(Math.sqrt(count))
				property int columns: (rows*rows < count) ? rows + 1 : rows
				anchors {
					left: parent.left
					right: parent.right
					top: filterItem.bottom
					bottom: parent.bottom
				}
				cellWidth: Math.floor(width / columns)
				cellHeight: Math.floor(height / rows)
				keyNavigationWraps: true
				model: clientModel
				highlight: PlasmaCore.FrameSvgItem {
					id: highlightItem
					imagePath: "widgets/viewitem"
					prefix: "hover"
					width: gridView.cellWidth
					height: gridView.cellHeight
				}
				boundsBehavior: Flickable.StopAtBounds
			}
		}
	}
}
