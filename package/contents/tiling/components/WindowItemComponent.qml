/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

Copyright (C) 2013 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
import QtQuick 1.1;
import org.kde.plasma.core 0.1 as PlasmaCore;
import org.kde.plasma.components 0.1 as Plasma;
import org.kde.qtextracomponents 0.1 as QtExtra;
import org.kde.kwin 0.1;

Item {
    signal requestClose
    property int standardMargin: 0
    property int standardLeftMargin: 0
    property int standardRightMargin: 0
    property int standardTopMargin: 0
    property int standardBottomMargin: 0
    property int highlightDuration: 200
    property variant view
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            workspace.activeClient = client;
            parent.requestClose();
        }
        onEntered: {
            view.currentIndex = index;
        }
    }
    ThumbnailItem {
        wId: client.windowId
        parentWindow: dialog.windowId
        brightness: (index == view.currentIndex) ? 1.0 : 0.4
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: captionItem.top
            leftMargin: standardLeftMargin
            rightMargin: standardRightMargin
            topMargin: standardTopMargin
            bottomMargin: standardMargin
        }
        Behavior on brightness {
            NumberAnimation {
                duration: highlightDuration
            }
        }
    }
    Item {
        id: captionItem
        height: childrenRect.height
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            leftMargin: standardLeftMargin + standardMargin
            bottomMargin: standardBottomMargin
            rightMargin: standardRightMargin
        }
        QtExtra.QPixmapItem {
            id: iconItem
            pixmap: client.icon
            width: 32
            height: 32
            anchors {
                bottom: parent.bottom
                right: textItem.left
            }
        }
        Item {
            id: textItem
            property int maxWidth: parent.width - iconItem.width - closeButton.width - parent.anchors.leftMargin - parent.anchors.rightMargin - anchors.leftMargin - standardMargin * 2
            width: (textElementSelected.implicitWidth >= maxWidth) ? maxWidth : textElementSelected.implicitWidth
            anchors {
                top: parent.top
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter
                leftMargin: standardMargin
            }
            Text {
                id: textElementSelected
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: client.caption
                font.italic: client.minimized
                font.bold: true
                visible: index == view.currentIndex
                color: theme.textColor
                elide: Text.ElideMiddle
                anchors.fill: parent
            }
            Text {
                id: textElementNormal
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: client.caption
                font.italic: client.minimized
                visible: index != view.currentIndex
                color: theme.textColor
                elide: Text.ElideMiddle
                anchors.fill: parent
            }
        }
        Plasma.Button {
            id: closeButton
            objectName: "closeButton"
            width: 32
            height: 32
            iconSource: "window-close"
            anchors {
                bottom: parent.bottom
                right: parent.right
            }
            onClicked: client.closeWindow()
            enabled: client.closeable
        }
    }
}
